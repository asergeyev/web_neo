from django.shortcuts import render
from django.http import HttpResponse
import pandas as pd
import json 

# Create your views here.

def home_view(request, *args, **kwargs):
	return render(request, 'testapp/home.html', {})

def contacts_view(request, *args, **kwargs):
	return render(request, 'testapp/contacts.html', {})

def neodata_view(request, *args, **kwargs):
	return render(request, 'testapp/neo_data.html', {})

def neo_table(request, *args, **kwargs):
    df = pd.read_csv("testapp/static/csv/neo_fin1.csv")

    json_records = df[:10].reset_index().to_json(orient ='records')
    columns = df.columns
    data = []
    data = json.loads(json_records)
    # context = {'d': data}
    context = {
             'data': data,
             'columns': columns
            }
  
    return render(request, 'testapp/table.html', context)